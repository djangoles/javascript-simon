const playContainer = document.querySelector('.play-container')
const buttons = document.querySelectorAll('.btn')
const startBtn = document.querySelector('.start-btn-1')

const gameFaceStatus = document.querySelector('.game-face-status')
const roundCountText = document.querySelector('.round-count-text')
const powerSwitch = document.getElementById('power')
const powerText = document.querySelector('.power-span')

const spanWin = document.querySelector('.span-win')
const spanLoss = document.querySelector('.span-loss')
const scoreContainer = document.querySelector('.score-container')

const appState = {
    round: 0,
    numberOfRounds: 4,
    loopCounter: 0,
    computerSequence: [],
    playerSequence: [],
    computerPlayCounter: 0,
    statusMSG: ['WINNER!', 'LOSER!', 'ERROR'],
    gameColors: ['GREEN', 'RED', 'YELLOW', 'BLUE'],
    intervalSpeed: 400,
    wins: 0,
    losses: 0
}

const updateRoundAppState = () => {
    appState.round = 0,
    appState.numberOfRounds+= 1,
    appState.loopCounter = 0,
    appState.computerSequence = [],
    appState.playerSequence = [],
    appState.computerPlayCounter = 0,
    appState.intervalSpeed = 400
}

const initState = () => {
    appState.round = 0,
    appState.numberOfRounds = 4,
    appState.loopCounter = 0,
    appState.computerSequence = [],
    appState.playerSequence = [],
    appState.computerPlayCounter = 0,
    appState.intervalSpeed = 400
    appState.wins = 0,
    appState.losses = 0
}

const buttonTrue = () => {
    buttons.forEach(button => {
        button.disabled = false
    }); 
}

const buttonFalse = () => {
    buttons.forEach(button => {
        button.disabled = true
    }); 
}

const computerPlay = () => {

    const random = Math.floor(Math.random() * 4)
    appState.computerSequence.push(random)
    // INCS NUMBER OF COMPUTER PLAYS 
    // i.e. how many times random num add was added to computerSequence
    appState.computerPlayCounter++
    gameFaceStatus.textContent = appState.round
    setTimeout(() => {
        // appState.round will always be = appState.computerSequence.length
        if(appState.round === appState.numberOfRounds) {
            gameFaceStatus.textContent = appState.statusMSG[0]
            appState.wins++
            spanWin.textContent = appState.wins
            const winAudio = document.getElementById('win-audio')
            winAudio.currentTime = 0
            winAudio.play()
            // RESET GAME STATE
            // PLAY HAPPY WIN IN SOUND
            updateRoundAppState()
            setTimeout(() => {
                computerPlay()
            }, 1000)
            // startBtn.disabled = false
            return
        }

        let computerPlaying = setInterval(() => {

            if(appState.loopCounter === appState.computerPlayCounter) {
                // CLEAR INTERVAL AND RESET appState.loopCounter
                // ****i.e end of a round******
                buttonTrue()
                window.clearInterval(computerPlaying);
                appState.loopCounter = 0
            } else {
                // ROUND CONTROL = appState.computerSequence.length
                appState.round = appState.computerSequence.length
                // NEED LIGHT UP AND SOUND FUNCTION HERE
                const button = buttons[appState.computerSequence[appState.loopCounter]]
                const buttonClass = button.classList[1] 
                const audio = document.querySelector(`audio[data-id="${appState.computerSequence[appState.loopCounter]}"]`);
                audio.currentTime = 0;
                if(audio) {
                    audio.play();
                    setTimeout(() => {
                        button.classList.toggle(`${buttonClass}-active`)
                    },20)

                    setTimeout(() => {
                        button.classList.toggle(`${buttonClass}-active`)
                    },200)
                    appState.loopCounter++
                }
            }
        }, appState.intervalSpeed)

    }, 0)
    appState.intervalSpeed = appState.intervalSpeed - 2
}

const humanPlay = (e) => {

    if(e.target.classList.contains('btn')) {
        // GRABS SELECTION FLASHES COLOR AND PLAYS SOUND===================
        const button = e.target
        appState.playerSequence.push(parseInt(button.dataset.id))

        const buttonClass = button.classList[1] 
        button.classList.toggle(`${buttonClass}-active`)
        const audio = document.querySelector(`audio[data-id="${button.dataset.id}"]`);
        audio.currentTime = 0;
        audio.play();
        setTimeout(() => {
            button.classList.toggle(`${buttonClass}-active`)
        },200)


        // CHECKING USER SELECTIONS ============================
        let matched = true
        for (let index = 0; index < appState.playerSequence.length; index++) {
            if(appState.playerSequence[index] !== appState.computerSequence[index]) {
                matched = false
                break
            }  
        }

        if(!matched) {
            // IF STRICT MODE THEN RESET GAME
            // ELSE LET PLAYER CONTINUE FROM LAST SEQ
            // PLAY ERROR LOSE SOUND
            const errorAudio = document.getElementById('error-audio')
            setTimeout(() => {
                errorAudio.currentTime = 0
                errorAudio.play()
                gameFaceStatus.textContent = appState.statusMSG[1]
                appState.losses++
                spanLoss.textContent = appState.losses
                updateRoundAppState()
                startBtn.disabled = false
                buttonFalse()
            }, 200)

        } else {
            if(appState.playerSequence.length === appState.round) {
                setTimeout(() => {
                    appState.playerSequence = []
                    appState.round++
                    gameFaceStatus.textContent = appState.round
                    buttonFalse()
                    computerPlay()
                }, 1000)
            }
        }
    }
  }

const playComputerLightsAndSound = () => {
    for (let index = 0; index < buttons.length; index++) {
        const button = buttons[index]
        const buttonClass = button.classList[1] 
        const audio = document.querySelector(`audio[data-id="${index}"]`);  
        if(audio) {
            setTimeout(() => {
                button.classList.toggle(`${buttonClass}-active`)
            },20)

            setTimeout(() => {
                button.classList.toggle(`${buttonClass}-active`)
            },200)
        }
    }
}

const updateScoreDisplay = () => {
    spanWin.textContent = appState.wins
    spanLoss.textContent = appState.losses
}


playContainer.addEventListener('click', humanPlay)

powerSwitch.addEventListener('change', (e)=> {
    initState()
    startBtn.disabled = false
    //TOGGLE COLOR ==========================
    if(e.target.checked === true ) {
        startBtn.disabled = false
        powerText.style.color = 'rgb(36, 139, 36)'
        gameFaceStatus.textContent = '---'
        playComputerLightsAndSound()
        scoreContainer.classList.add('score-container-show')
    } else {
        startBtn.disabled = true
        gameFaceStatus.textContent = 'GOODBYE!'
        gameFaceStatus.style.color = 'red'
        powerText.style.color = '#fff'
        buttonFalse()
        scoreContainer.classList.remove('score-container-show')
    }
    setTimeout(() => {
        gameFaceStatus.classList.toggle('show-game-face-status')
        roundCountText.classList.toggle('show-game-face-status')
        updateScoreDisplay()
    }, 300)
})

startBtn.addEventListener('click',  () => {
    startBtn.classList.add('start-btn-1-show')
    appState.round++
    gameFaceStatus.textContent = appState.round
    startBtn.disabled = true
    setTimeout(() => {
        startBtn.classList.remove('start-btn-1-show')
        computerPlay()
    }, 100)
})

window.addEventListener('load', () => {
    buttonFalse()
    updateScoreDisplay()
    startBtn.disabled = true
})





  
